from json import loads
from django.views import View
from django.shortcuts import render
from django.views.generic import TemplateView
from imageReaderByHsv.models import UploadImage
from imageReaderByHsv.forms import ReaderImageForm


class HomeView(TemplateView):
    template_name = "image_reader_by_hsv/index.html"


class ServicesView(TemplateView):
    template_name = "image_reader_by_hsv/services.html"


class ReaderImageView(View):
    form_class = ReaderImageForm
    template_name = "image_reader_by_hsv/reader_image.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        form_data = {
            'form': form,
        }
        if form.is_valid():
            if not UploadImage.objects.filter(uuid=request.POST.get("uuid", "N/A")):
                form.full_clean()
                hsv_parameters = loads(form.cleaned_data.get('hsv_parameters'))
                prom_hsv_degree = 0
                prom_hsv_saturation = 0
                for key in hsv_parameters:
                    prom_hsv_degree += int(hsv_parameters.get(key)[0])
                    prom_hsv_saturation += int(hsv_parameters.get(key)[1])
                    classification = UploadImage().calculate_classification(int(hsv_parameters.get(key)[0]),
                                                                            int(hsv_parameters.get(key)[1]))
                    form_data = {**form_data, **{
                        key: classification,
                        f"{key}_color": f"hsv({hsv_parameters.get(key)[0]},{hsv_parameters.get(key)[1]}%,"
                                        f"{hsv_parameters.get(key)[2]}%)",
                    }, **form.cleaned_data}

                classification_prom = UploadImage().calculate_classification(prom_hsv_degree / 4,
                                                                             prom_hsv_saturation / 4)
                coordinate_parameter = loads(form.cleaned_data.get('coordinate_parameter'))
                for index in range(coordinate_parameter.__len__()):
                    form_data = {**form_data, **{
                        f"coordinate{index + 1}": f"X: {coordinate_parameter[index]['x']}, "
                                                  f"Y: {coordinate_parameter[index]['y']}",
                    }, **form.cleaned_data}

                upload_image = form.save_params(form_data)

                form_data = {**form_data, **{
                    'image_name': upload_image.image.name,
                    'hsv_prom': classification_prom,
                    'pk': upload_image.pk
                }, **form.cleaned_data}
        else:
            return render(request, self.template_name, form_data, status=400)
        return render(request, self.template_name, form_data)
