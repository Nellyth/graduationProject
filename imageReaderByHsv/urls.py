from django.conf.urls import url
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from django.views.static import serve

from graduationProject import settings
from imageReaderByHsv.views import HomeView, ServicesView, ReaderImageView

app_name = 'Image Reader By Hsv'

urlpatterns = [
    path('', HomeView.as_view(), name="HomeView"),
    path('services', ServicesView.as_view(), name="ServicesView"),
    path('reader-image', ReaderImageView.as_view(), name="ReaderImageView"),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
