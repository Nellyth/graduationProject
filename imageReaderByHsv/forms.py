import json, base64
from json import loads
from django import forms
from matplotlib import image
from matplotlib import pyplot as plt
from django.core.files.base import ContentFile
from imageReaderByHsv.models import UploadImage


class ReaderImageForm(forms.ModelForm):
    hsv_parameters = forms.CharField(required=True, max_length=200)
    coordinate_parameter = forms.CharField(required=True, max_length=200)
    image_test = forms.CharField(required=True)
    image = forms.ImageField(
        required=True,
        widget=forms.FileInput(attrs={'class': 'form-control'})
    )
    classification = forms.ChoiceField(
        required=False,
        choices=UploadImage().CHOICE_CLASSIFICATION
    )

    class Meta:
        model = UploadImage
        fields = ['image', 'hsv_parameters', 'classification', 'uuid', 'image_test']

    def clean_hsv_parameters(self):
        try:
            json_data = json.loads(self.cleaned_data.get("hsv_parameters"))
        except json.decoder.JSONDecodeError as e:
            json_data = {}

        if not json_data or json_data.__len__() != 4:
            raise forms.ValidationError(
                "Invalid data, it is necessary to select 4 colored inputs."
            )

        if self.get_number_of_parameters_inside_the_json(json_data) != 4:
            raise forms.ValidationError(
                "Invalid data, the information is incomplete or corrupt."
            )

        self.cleaned_data['classification'] = self.get_classification(
            json.loads(self.cleaned_data.get('hsv_parameters'))
        )

        return self.cleaned_data.get("hsv_parameters")

    def clean_coordinate_parameter(self):
        try:
            json_data = json.loads(self.cleaned_data.get("coordinate_parameter"))
        except json.decoder.JSONDecodeError as e:
            json_data = {}

        if not json_data or json_data.__len__() != 4:
            raise forms.ValidationError("Invalid data, it is necessary to receive 4 coordinates.")

        if tuple(filter(lambda x: list(x.keys()) == ['x', 'y'], json_data)).__len__() != 4:
            raise forms.ValidationError("Invalid data, the information is incomplete or corrupt.")

        return self.cleaned_data.get("coordinate_parameter")

    @staticmethod
    def get_number_of_parameters_inside_the_json(json_data):
        return tuple(
            filter(
                lambda x: json_data[x].__len__() == 3, json_data
            )
        ).__len__()

    def clean(self):
        cleaned_data = super(ReaderImageForm, self).clean()

        return cleaned_data

    @staticmethod
    def get_classification(input_params):
        hsv_prom = UploadImage().calculate_averaged_classification(input_params)
        return tuple(filter(lambda x: x[1] == hsv_prom, UploadImage.CHOICE_CLASSIFICATION))[0][0]

    @staticmethod
    def update_image(instance):
        data = image.imread(instance.image_test.path)
        for coordinate in loads(instance.coordinate_parameter):
            plt.scatter(coordinate.get("x"), coordinate.get("y"), s=7, marker='.', color="red")
        plt.imshow(data)
        plt.savefig(instance.image_test.path)

    def save_params(self, cleaned_data):
        upload_image, created = UploadImage.objects.get_or_create(
            uuid=cleaned_data.get("uuid"),
            defaults={
                'image': cleaned_data.get('image'),
                'hsv_parameters': cleaned_data.get("hsv_parameters"),
                'coordinate_parameter': cleaned_data.get("coordinate_parameter"),
                'classification': UploadImage.ROJO
            }
        )

        if created:
            data = ContentFile(base64.b64decode(self.cleaned_data.get("image_test")))
            upload_image.image_test.save("test.png", data, save=True)
            self.update_image(upload_image)

        return upload_image
