import json
from io import BytesIO

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from unittest import TestSuite, TestLoader

from django.urls import reverse
from pyunitreport import HTMLTestRunner
from imageReaderByHsv.forms import ReaderImageForm
from imageReaderByHsv.models import UploadImage
from PIL import Image


class ReaderImageFormTest(TestCase):
    def setUp(self):

        # Crear una imagen de 1x1 píxel blanca
        img = Image.new('RGB', (1, 1), color='white')
        img_bytes = BytesIO()
        img.save(img_bytes, format='png')
        img_bytes.seek(0)

        self.image_file = SimpleUploadedFile('test_image.png', img_bytes.read(), content_type='image/png')
        self.form_data = {
            'hsv_parameters': json.dumps(
                {"hsv1": ["41", "58", "100"], "hsv2": ["27", "71", "96"], "hsv3": ["23", "64", "89"],
                 "hsv4": ["22", "70", "100"]}),
            'coordinate_parameter': json.dumps(
                [{"x": 756, "y": 219}, {"x": 821, "y": 273}, {"x": 747, "y": 297}, {"x": 750, "y": 255}]),
            'image_test': 'R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=',
            'image': self.image_file,
            'classification': UploadImage.ROJO
        }

    def test_valid_form(self):
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertTrue(form.is_valid())

    def test_invalid_hsv_parameters(self):
        self.form_data['hsv_parameters'] = json.dumps({})
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['hsv_parameters'][0],
            'Invalid data, it is necessary to select 4 colored inputs.'
        )

    def test_invalid_coordinate_parameter(self):
        self.form_data['coordinate_parameter'] = json.dumps({})
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['coordinate_parameter'][0],
            'Invalid data, it is necessary to receive 4 coordinates.'
        )

    def test_invalid_classification(self):
        self.form_data['hsv_parameters'] = json.dumps({'color1': [0, 0, 0], 'color2': [255, 255, 255]})
        self.form_data['classification'] = 'invalid_classification'
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['classification'][0],
            'Select a valid choice. invalid_classification is not one of the available choices.'
        )

    def test_clean_method(self):
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertTrue(form.is_valid())
        cleaned_data = form.clean()
        self.assertEqual(cleaned_data['classification'], UploadImage.ROJO)

    def test_clean_hsv_parameters(self):
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertTrue(form.is_valid())
        cleaned_data = form.clean_hsv_parameters()
        self.assertEqual(cleaned_data, self.form_data['hsv_parameters'])

    def test_clean_coordinate_parameter(self):
        form = ReaderImageForm(data=self.form_data, files={'image': self.form_data['image']})
        self.assertTrue(form.is_valid())
        cleaned_data = form.clean_coordinate_parameter()
        self.assertEqual(cleaned_data, self.form_data['coordinate_parameter'])

    def test_image_field_required(self):
        form = ReaderImageForm(data=self.form_data, files={})
        self.assertFalse(form.is_valid())
        self.assertIn('image', form.errors)


class HomeViewTestCase(TestCase):
    def test_home_view(self):
        url = reverse('Image Reader By Hsv:HomeView')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'image_reader_by_hsv/index.html')


class ServicesViewTestCase(TestCase):
    def test_services_view(self):
        url = reverse('Image Reader By Hsv:ServicesView')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'image_reader_by_hsv/services.html')


class ReaderImageViewTestCase(TestCase):
    def setUp(self):
        self.url = reverse('Image Reader By Hsv:ReaderImageView')
        # Crear una imagen de 1x1 píxel blanca
        img = Image.new('RGB', (1, 1), color='white')
        img_bytes = BytesIO()
        img.save(img_bytes, format='png')
        img_bytes.seek(0)

        self.image_file = SimpleUploadedFile('test_image.png', img_bytes.read(), content_type='image/png')
        self.form_data = {
            'hsv_parameters': json.dumps(
                {"hsv1": ["41", "58", "100"], "hsv2": ["27", "71", "96"], "hsv3": ["23", "64", "89"],
                 "hsv4": ["22", "70", "100"]}),
            'coordinate_parameter': json.dumps(
                [{"x": 756, "y": 219}, {"x": 821, "y": 273}, {"x": 747, "y": 297}, {"x": 750, "y": 255}]),
            'image_test': 'R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=',
            'image': self.image_file,
            'classification': UploadImage.ROJO
        }

    def test_get_reader_image_view(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'image_reader_by_hsv/reader_image.html')

    def test_post_reader_image_view_with_invalid_data(self):
        data = {
            'hsv_parameters': 'invalid_json',
            'coordinate_parameter': 'invalid_json',
            'image': self.image_file,
            'uuid': 'abcd-1234'
        }

        response = self.client.post(self.url, data=data, format='multipart')

        self.assertEqual(response.status_code, 400)
        self.assertTemplateUsed(response, 'image_reader_by_hsv/reader_image.html')

        self.assertFalse(UploadImage.objects.filter(uuid='abcd-1234').exists())

    def test_post_reader_image_view_with_invalid_image(self):
        data = {
            'hsv_parameters': '{"0": [10, 20, 30], "1": [40, 50, 60], "2": [70, 80, 90], "3": [100, 110, 120]}',
            'coordinate_parameter': '[{"x": 10, "y": 20}, {"x": 30, "y": 40}, {"x": 50, "y": 60}]',
            'image': SimpleUploadedFile('test.txt', b'This is not an image.'),
            'uuid': 'abcd-1234'
        }

        response = self.client.post(self.url, data=data, format='multipart')

        self.assertEqual(response.status_code, 400)

    def test_post_reader_image_view_with_missing_image(self):
        data = {
            'hsv_parameters': '{"0": [10, 20, 30], "1": [40, 50, 60], "2": [70, 80, 90], "3": [100, 110, 120]}',
            'coordinate_parameter': '[{"x": 10, "y": 20}, {"x": 30, "y": 40}, {"x": 50, "y": 60}]',
            'uuid': 'abcd-1234'
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 400)


reader_image_form_test = TestLoader().loadTestsFromTestCase(ReaderImageFormTest)
home_view_test = TestLoader().loadTestsFromTestCase(HomeViewTestCase)
service_view_test = TestLoader().loadTestsFromTestCase(ServicesViewTestCase)
reader_image_view_test = TestLoader().loadTestsFromTestCase(ReaderImageViewTestCase)

test_suite_object = TestSuite([reader_image_form_test, home_view_test, service_view_test, reader_image_view_test])

kwargs = {'output': 'TestsReport', 'report_name': 'TestsReport', 'report_title': 'Graduation Project Tests'}

runner = HTMLTestRunner(**kwargs)
runner.run(test_suite_object)
