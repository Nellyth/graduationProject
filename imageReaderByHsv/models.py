import os
import json
from django.db import models


def get_upload_path(instance, filename):
    import datetime
    month = datetime.datetime.today().month
    year = datetime.datetime.today().year
    day = datetime.datetime.today().day
    return "image_reader_hsv/%s/%s/%s/%s" % (year, month, day, os.path.basename(filename))


class UploadImage(models.Model):
    ROJO = "rojo"
    NORMAL = "normal"
    LEVE_PALIDEZ = "leve palidez"
    PALIDEZ_MARCADA = "palidez marcada"
    PALIDEZ_CEREA = "palidez cerea"
    SIN_CLASIFICACION = "Sin Clasificación"

    CHOICE_CLASSIFICATION = (
        (ROJO, 'Rojo oscuro'),
        (NORMAL, 'Rojo'),
        (LEVE_PALIDEZ, 'Rojo calido'),
        (PALIDEZ_MARCADA, 'Naranja'),
        (PALIDEZ_CEREA, 'Amarillo calido'),
        (SIN_CLASIFICACION, 'Sin Clasificación'),
    )

    upload_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=get_upload_path)
    coordinate_parameter = models.CharField(max_length=200)
    hsv_parameters = models.CharField(max_length=200)
    image_test = models.ImageField(upload_to=get_upload_path)
    classification = models.CharField(max_length=50, choices=CHOICE_CLASSIFICATION)
    uuid = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return f"Classification: {self.classification} - Parameters: {self.hsv_parameters}"

    def calculate_classification(self, degree: float, saturation: float):
        # crea un diccionario que contiene las opciones de clasificación posibles
        choice_classification = dict(self.CHOICE_CLASSIFICATION)

        # verifica si los valores de matiz y saturación cumplen con ciertas condiciones para cada una de las
        # clasificaciones posibles
        if 0 <= degree <= 9.9 and 80 <= saturation <= 100:
            # si los valores cumplen con las condiciones, obtén la clasificación correspondiente del diccionario
            classification = choice_classification.get(self.ROJO)
        elif 10 <= degree <= 19.9 and 60 <= saturation <= 100:
            classification = choice_classification.get(self.NORMAL)
        elif 20 <= degree <= 29.9 and 40 <= saturation <= 100:
            classification = choice_classification.get(self.LEVE_PALIDEZ)
        elif 30 <= degree <= 39.9 and 20 <= saturation <= 100:
            classification = choice_classification.get(self.PALIDEZ_MARCADA)
        elif 40 <= degree <= 49.9 and 0 <= saturation <= 100:
            classification = choice_classification.get(self.PALIDEZ_CEREA)
        else:
            # si los valores no cumplen con ninguna de las condiciones, devuelve "Sin Clasificación"
            classification = "Sin Clasificación"

        # devuelve la clasificación correspondiente o "Sin Clasificación" si no se cumplen las condiciones
        return classification

    def get_parameter_classification(self):
        classifications_dictionary = {}
        json_params = self.get_json_from_hsv_parameters()
        if isinstance(json_params, dict):
            for key in json_params:
                classification = self.calculate_classification(int(json_params.get(key)[0]),
                                                               int(json_params.get(key)[1]))
                classifications_dictionary.setdefault(key, classification)
        return classifications_dictionary

    def get_json_from_hsv_parameters(self):
        if self.hsv_parameters:
            try:
                return json.loads(self.hsv_parameters)
            except json.decoder.JSONDecodeError:
                return ValueError("The value cannot be parsed to a Json type.")

    def calculate_averaged_classification(self, input_params: dict):
        degree = 0
        saturation = 0
        for key in input_params.keys():
            degree += int(input_params.get(key)[0])
            saturation += int(input_params.get(key)[1])
        degree /= input_params.__len__()
        saturation /= input_params.__len__()
        return self.calculate_classification(degree, saturation)
