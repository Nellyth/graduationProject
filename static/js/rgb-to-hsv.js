function rgb2hsv(rgb) {
    let rabs, gabs, babs, rr, gg, bb, h, s, v, diff, diffc, percentRoundFn;

    if (rgb instanceof Array) {
        rabs = rgb[0] / 255;
        gabs = rgb[1] / 255;
        babs = rgb[2] / 255;
    } else {
        rabs = arguments[0] / 255;
        gabs = arguments[1] / 255;
        babs = arguments[2] / 255;
    }

    v = Math.max(rabs, gabs, babs),
        diff = v - Math.min(rabs, gabs, babs);
    diffc = c => (v - c) / 6 / diff + 1 / 2;
    percentRoundFn = num => Math.round(num * 100) / 100;
    if (diff === 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(rabs);
        gg = diffc(gabs);
        bb = diffc(babs);

        if (rabs === v) {
            h = bb - gg;
        } else if (gabs === v) {
            h = (1 / 3) + rr - bb;
        } else if (babs === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return [Math.round(h * 360), percentRoundFn(s * 100), percentRoundFn(v * 100)];
}

function validateHsv(HSV) {
    return /^hsv\((\s*\d{1,3}\s*),(\s*\d{1,3}%\s*),(\s*\d{1,3}%\s*)\)$/.test(HSV);
}

function display_hsv(ry) {
    const hsv = "hsv(" + Math.round(ry[0]) + "," + Math.round(ry[1]) + "%," + Math.round(ry[2]) + "%)";
    if (validateHsv(hsv)) {
        return hsv;
    } else {
        return false;
    }
}