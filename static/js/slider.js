document.addEventListener('DOMContentLoaded', function() {
  // Query the elements
  const image = document.getElementById('img');
  const slider = document.getElementById('myRange');
  const minScale = 1;
  const maxScale = 2;
  const step = (maxScale - minScale) / 100;

  slider.oninput = function() {
    const scale = minScale + this.value * step;
    image.style.transform = `scale(${scale}, ${scale})`;
  }
});

// $("#videoContainer").css('height', $('#img').height());
// $("#videoContainer").css('width', $('#img').width());

$('#videoContainer').on('mousewheel', function (event) {
    var height = $('#img').height();
    var width = $('#img').width();

    if (height == 480 && width == 640 && event.deltaY > 0) {
    } else {
        if (event.deltaY > 0) {
            height /= 2;
            width /= 2;
            $("#img").css('height', height);
            $("#img").css('width', width);
        }
        else if (event.deltaY < 0) {
            height *= 2;
            width *= 2;
            $("#img").css('height', height);
            $("#img").css('width', width);
        }
    }
});

function startDrag(e) {
    if (!e) {
        var e = window.event;
    }

    var targ = e.target ? e.target : e.srcElement;

    if (targ.className !== 'dragme') {
        return
    }
    offsetX = e.clientX;
    offsetY = e.clientY;

    if (!targ.style.left) {
        targ.style.left = '0px'
    }
    if (!targ.style.top) {
        targ.style.top = '0px'
    }
    coordX = parseInt(targ.style.left);
    coordY = parseInt(targ.style.top);
    drag = true;

    document.onmousemove = dragDiv;
    return false;
}
function dragDiv(e) {
    if (!drag) {
        return
    }
    if (!e) {
        var e = window.event
    }
    var targ = e.target ? e.target : e.srcElement;
    // move div element
    targ.style.left = coordX + e.clientX - offsetX + 'px';
    targ.style.top = coordY + e.clientY - offsetY + 'px';
    return false;
}
function stopDrag() {
    drag = false;
}
window.onload = function () {
    document.onmousedown = startDrag;
    document.onmouseup = stopDrag;
}