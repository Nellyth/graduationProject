/*INCLUDES:
Lea Verou's color contrast:
https://codepen.io/enxaneta/pen/729bdb57bcace876689066ba81417fc7
hsl to rgb to hex:
https://codepen.io/enxaneta/pen/15d04eb1b8b68c95cd5298b46b2eabb8
*/

function getFontColor(rgbRy) {
    if (colorContrast(rgbRy, [255, 255, 255]) > 4.5) {
        return "white";
    } else {
        return "black";
    }
}

let theBody = document.body;
const palette = document.querySelector(".palette");
let img = document.getElementById("img");

const viewColor = document.querySelector(".viewColor");// the current color
const colorsRy = [];

let width = document.getElementById("imagePreview").offsetWidth;
let height = 800;

let canvas = document.createElement("canvas");
const ctx = canvas.getContext("2d");

//Parameters Required
let imgData;
let pixels;
let thisRGB;
let thisRGBRy;

try {
    const objImage = new Image();
    objImage.crossOrigin = "*";
    objImage.src = img.src;
    objImage.onload = function (ev) {
        $(".viewColor").removeClass("hide");
        $(".selectColor").removeClass("hide");
        $(".slidecontainer").removeClass("hide");

        height = ev.srcElement.height;
        if (ev.srcElement.height > 800) {
            height = 800;
        }

        canvas.width = width;
        canvas.height = height;
        ctx.drawImage(objImage, 0, 0, width, height);

        // document.getElementById('img').src = img.src;

        imgData = ctx.getImageData(0, 0, width, height);
        pixels = imgData.data;
    }
} catch (e) {
    $(".viewColor").addClass("hide");
    $(".selectColor").addClass("hide");
    $(".slidecontainer").addClass("hide");
}

// thisRGB;
// thisRGBRy;

// on mousemove you get the current color
// img.addEventListener("mousemove", function (e) {
//     const m = oMousePos(img, e);
//
//     imgData = ctx.getImageData(0, 0, width, height);
//     pixels = imgData.data;
//     const i = (m.x + m.y * img.offsetWidth) * 4;
//     const R = pixels[i];
//     const G = pixels[i + 1];
//     const B = pixels[i + 2];
//     thisRGBRy = [R, G, B];
//     thisRGB = display_rgb(thisRGBRy);
//     viewColor.style.backgroundColor = thisRGB;
//     viewColor.style.color = getFontColor(thisRGBRy);
//     //viewColor.innerHTML =  thisRGB;
//
// }, false);

// You may drag and drop a new image of your choice.
theBody.addEventListener("dragenter", dragenter, false);
theBody.addEventListener("dragover", dragover, false);
theBody.addEventListener("drop", drop, false);

function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
}

function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
}

function drop(e) {
    e.stopPropagation();
    e.preventDefault();

    const datos = e.dataTransfer;
    const theFiles = datos.files;

    handleFiles(theFiles);
}

function handleFiles(theFiles) {
    for (let i = 0; i < theFiles.length; i++) {
        const _file = theFiles[i];
        const isImg = /^image\//;

        if (!isImg.test(_file.type)) {
            continue;
        }

        const img = new Image();
        img.src = window.URL.createObjectURL(_file);
        img.onload = function (ev) {
            $(".viewColor").removeClass("hide");
            $(".selectColor").removeClass("hide");
            $(".slidecontainer").removeClass("hide");

            height = ev.srcElement.height;
            if (ev.srcElement.height > 800) {
                height = 800;
            }

            canvas.width = width;
            canvas.height = height;

            ctx.drawImage(this, 0, 0, width, height);

            dataurl = canvas.toDataURL(_file.type);
            document.getElementById('img').src = dataurl;

            imgData = ctx.getImageData(0, 0, width, height);
            pixels = imgData.data;
        }
    }
}

// END drag and drop new image


function Swatch(RGBry, parent) {
    this.element = document.createElement("div");

    this.rgb = display_rgb(RGBry);
    this.hex = display_hex(rgb2hex(RGBry));
    this.hsv = display_hsv(rgb2hsv(RGBry));
    this.hsl = display_hsl(rgb2hsl(RGBry));

    this.att = {}
    this.att.class = "swatch";
    this.att.style = "background-color:" + this.rgb + "; color:" + getFontColor(RGBry) + ";";
    for (const name in this.att) {
        if (this.att.hasOwnProperty(name)) {
            this.element.setAttribute(name, this.att[name]);
        }
    }
    this.element.innerHTML = this.hex + "<br>" + this.rgb + "<br>" + this.hsv + "<br>" + this.hsl;
    parent.appendChild(this.element)
}

let inputColor = document.getElementById("inputColor")
inputColor.addEventListener("change", function (e) {

    let colorRGB = hexToRgb(inputColor.value)
    thisRGBRy = [colorRGB.r, colorRGB.g, colorRGB.b];

    // add swatch on click
    if (colorsRy.length < 4) {
        const swatch = new Swatch(thisRGBRy, palette);
        colorsRy.push(swatch);
        // get the colors string
        const colorsStr = getColorsStr(colorsRy);
        console.clear();
        console.log(colorsStr);
    }
}, false);

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}


palette.addEventListener("dblclick", function (e) {
    // remove swatch on dblclick
    if (e.target.className === "swatch") {
        for (let i = 0; i < colorsRy.length; i++) {
            if (colorsRy[i].element === e.target) {
                colorsRy.splice(i, 1);
                palette.removeChild(e.target);
                break;
            }
        }
    }

    const colorsStr = getColorsStr(colorsRy);
    console.clear();
    console.log(colorsStr);

}, false);

function clearSwatches(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function getColorsStr(colorsRy) {
    let colorsStr = ''
    for (let i = 0; i < colorsRy.length; i++) {
        colorsStr += '[' + colorsRy[i].hex + ',' + colorsRy[i].rgb + ',' + colorsRy[i].hsl + ']';
        if (i < colorsRy.length - 1) {
            colorsStr += ',\n';
        }
    }
    return colorsStr;
}

function oMousePos(canvas, evt) {
    const ClientRect = canvas.getBoundingClientRect();
    return { //objeto
        x: Math.round(evt.clientX - ClientRect.left),
        y: Math.round(evt.clientY - ClientRect.top)
    }
}