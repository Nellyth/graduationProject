img = document.getElementById("image");
img.addEventListener('change', function (ev) {
    let canvas;
    let ctx;
    if (ev.target.files) {
        let file = ev.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = function (e) {
                let customImg = document.createElement("img");
                customImg.src = e.target.result;

                canvas = document.createElement("canvas");
                ctx = canvas.getContext("2d");

                let objImage = new Image();
                objImage.src = e.target.result;
                objImage.onload = function (ev) {
                    $(".viewColor").removeClass("hide");
                    $(".selectColor").removeClass("hide");
                    $(".slidecontainer").removeClass("hide");

                    var width = document.getElementById("imagePreview").offsetWidth;
                    var height = ev.srcElement.height;
                    if (ev.srcElement.height > 800) {
                        height = 800;
                    }

                    canvas.width = width;
                    canvas.height = height;
                    ctx = canvas.getContext("2d");
                    ctx.drawImage(customImg, 0, 0, width, height);

                    dataurl = canvas.toDataURL(file.type);
                    document.getElementById('img').src = dataurl;
                    let resizeImg = document.getElementById('img')

                    imgData = ctx.getImageData(0, 0, width, height);
                    pixels = imgData.data;
                }
            }
        }
    }
});

$(document).ready(function () {
    $("#button_process").on("click", function (event) {
        const regExp = /\(([^)]+)\)/;
        for (let i = 0; i < colorsRy.length; i++) {
            input_params["hsv" + (i + 1)] = regExp.exec(colorsRy[i].hsv)[1].replace(
                "(", ""
            ).replace(
                ")", ""
            ).replaceAll(
                "%", ""
            ).split(",");
        }

        document.getElementById("input_params").value = JSON.stringify(input_params);
    });
});
