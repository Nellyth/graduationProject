# Graduation Project
## Index
* [Install and Use Docker on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)
* [Install and Use Docker Compose on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04)
* [Quickstart: Compose and Django](#quickstart-compose)

### Generating project
```shell
cd /home
mkdir project
cd project
git clone https://gitlab.com/Nellyth/graduationProject.git -b master
cd graduationProject
touch local_settings.py
nano local_settings.py
```

Paste the following information into the local_settings.py file
```python
DATABASES = {
        'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'graduationProject',
                'USER': 'postgres',
                'PASSWORD': 'postgres',
                'HOST': 'db',
                'PORT': '5432',
        }
}
```
We save and close the file local_settings.py

```shell
touch Dockerfile
nano Dockerfile
```

Paste the following information into the Dockerfile file
```shell
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY graduationProject/requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
```
We save and close the file Dockerfile

```shell
touch docker-compose.yml
nano docker-compose.yml
```

Paste the following information into the docker-compose.yml file
```shell
version: "3.9"

services:
  db:
    image: postgres
    volumes:
      - ./data/db:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=graduationProject
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
  web:
    build: .
    command: python manage.py runserver 0.0.0.0:8000 ----insecure
    volumes:
      - ./graduationProject:/code
    ports:
      - "8000:8000"
    environment:
      - POSTGRES_NAME=graduationProject
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
    depends_on:
      - db
```

We run the server
```shell
docker compose up -d
```

We verify that the server is active and also we get the name of the web container
```shell
docker ps
```
####Example: project-web-1
We enter the shell of the container with its respective name
```shell
docker exec -ti project-web-1 /bin/bash
```

We create the database
```shell
python manage.py migrate
```

We create the superuser
```shell
python manage.py createsuperuser
---------------Example------------------
Username (leave blank to use 'root'): admin
Email address: 
Password: admin
Password (again): admin
The password is too similar to the username.
This password is too short. It must contain at least 8 characters.
This password is too common.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
---------------End Example------------------
```

We generate the static files
```shell
python manage.py collectstatic
```

We exit the container shell
```shell
exit
```

Once all the previous steps have been completed correctly, we can enter our server through the server's IP and port 8000.

###Ejemplo para entrar a la pagina de administracion 
https://localhost:8000/admin