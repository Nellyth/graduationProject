from django.apps import AppConfig


class ImageReaderByHsvConfig(AppConfig):
    name = 'imageReaderByHsv'
    verbose_name = "Image Reader By Hsv"
